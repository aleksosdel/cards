class Cards {
  constructor(src, title, desc) {
    this.src = src,
    this.title = title,
    this.desc = desc,
    this.parent = document.querySelector(".parent")
  }
  render() {
    const card = document.createElement('div')
    card.classList.add('card', 'col-4', 'mt-1' )
    card.innerHTML = `
      <img src=${this.src} alt="" class="card__img">
      <div class="card__title">${this.title}</div>
      <div class="card__descr">${this.desc}</div>
      <button class="card__btn btn btn-primary">Buy</button>
    `
    this.parent.append(card)
  }
}

new Cards(
  'https://www.gastronom.ru/binfiles/images/20191113/bd570867.jpg',
  'Five Cheese Pizza',
  'You are here: Home / Appetizer / FIVE CHEESE PIZZA FIVE CHEESE PIZZA May 22, 2018 by Kushi 11 Comments Five Cheese Pizza is a classic recipe prepared using marinara sauce, my favorite 5 cheeses and seasonings and fresh homemade pizza dough.'
).render()